# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    up_case = False
    lo_case = False
    has_digi = False
    has_special = False

    for c in password:
        if c.isalpha():
            if c.islower():
                up_case = True
            else:
                lo_case = True
        elif c.isdigit():
            has_digi = True
        elif c == "!" or "@" or "#" or "$" or "&":
            has_special = True
    return (len(password) >= 6 and len(password) <= 12
            and lo_case
            and up_case
            and has_digi
            and has_special
            )


user_in = input("Whats the password!")
user_in = str(user_in)
print(check_password(user_in))
