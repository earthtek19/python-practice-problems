# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(lst_in):
    lis1 = []
    lis2 = []
    lst_in_length = len(lst_in) // 2 + (len(lst_in) % 2)
    for i in range(lst_in_length):
        lis1.append(lst_in[i])
    for i in range(len(lst_in) // 2):
        index = i + lst_in_length
        lis2.append(lst_in[index])
    return lis1, lis2
