# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    numbers = [value1, value2, value3]
    max_number = max(numbers)
    return (max_number)


max_number = max_of_three(3, 5, 1)

print(f'The max number is {max_number}')
