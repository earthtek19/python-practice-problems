# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    pasta = ["flour", "eggs", "oil"]
    return all(ingredient in ingredients for ingredient in pasta)


ing_lst = ["eggs", "oil", "flour"]
spaghett = can_make_pasta(ing_lst)
print(spaghett)
